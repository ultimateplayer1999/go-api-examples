package main

import (
	"encoding/json"
	"net/http"
	"log"
	"fmt"
	"os"
	"io/ioutil"

	"github.com/joho/godotenv"
)

type Response struct {
	Name string `json:"name"`
	IPAddress string `json:"IPAddress"`
	MACAddress string `json:"MacAddress"`
	Memory string `json:"memory"`
	CPUs string `json:"cpus"`
	restartPolicy struct {
		Name string `json:"Name"`
		MaxRetry int `json:"MaximumRetryCount"`
	} `json:"restartPolicy"`
	Restarts int `json:"restarts"`
	State struct {
		Status string `json:"Status"`
		Running bool `json:"Running"`
		Paused bool `json:"Paused"`
		Restarting bool `json:"Restarting"`
		OOMKilled bool `json:"OOMKilled"`
		Dead bool `json:"Dead"`
		PID int `json:"Pid"`
		ExitCode int `json:"ExitCode"`
		Err string `json:"Error"`
		StartedAt string `json:"StartedAt"`
		FinishedAt string `json:"FinishedAt"`
	} `json:"state"`
	Created string `json:"created"`
	Image string `json:"image"`
}

func main() {
	// Clear the screen
	fmt.Print("\033[2J")
	// Load environment variables from .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	token := os.Getenv("TOKEN")

	// Make an HTTP request to api.discord-linux.com
	req, err := http.NewRequest("GET", "https://api.discord-linux.com/info", nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Add("Content-Type", "Application/json")
	req.Header.Add("x-discord-linux-auth", token)

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	// Get the statuscode of the endpoint
	fmt.Println("Status code: ", resp.StatusCode)

	// Read the contents of the body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Unmarshal the JSON into a Response struct
	var r Response
	err = json.Unmarshal(body, &r)
	if err != nil {
		log.Fatal(err)
	}

	// Print the response
	fmt.Println("")
	fmt.Println("Containername:", r.Name)
	fmt.Println("MAX Memory:", r.Memory)
	fmt.Println("CPU amount:", r.CPUs)
	fmt.Println("Restarts:", r.Restarts)
	fmt.Println("Status:", r.State.Status)
	fmt.Println("PID:", r.State.PID)
	fmt.Println("StartedAt:", r.State.StartedAt)
	fmt.Println("FinishedAt:", r.State.FinishedAt)
	fmt.Println("Created:", r.Created)
	fmt.Println("")
}
