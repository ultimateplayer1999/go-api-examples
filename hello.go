package main

import (
	"encoding/json"
	"net/http"
	"log"
	"fmt"
	"os"
	"io/ioutil"

	"github.com/joho/godotenv"
)

type Response struct {
	Message string `json:"message"`
}

func main() {
	// Clear the screen
	fmt.Print("\033[2J")
	// Load environment variables from .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	token := os.Getenv("TOKEN")

	// Make an HTTP request to api.discord-linux.com
	req, err := http.NewRequest("GET", "https://api.discord-linux.com/hello", nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Add("Content-Type", "Application/json")
	req.Header.Add("x-discord-linux-auth", token)

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	// Get the statuscode of the endpoint
	fmt.Println("Status code: ", resp.StatusCode)

	// Read the contents of the body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Unmarshal the JSON into a Response struct
	var r Response
	err = json.Unmarshal(body, &r)
	if err != nil {
		log.Fatal(err)
	}

	// Print the response
	fmt.Println("")
	fmt.Println(r.Message)
	fmt.Println("")

}
