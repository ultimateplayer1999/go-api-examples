Some examples for API usage in Go for the ssh.surf API.

All of them based on a .env file.

To install this package you can use the following while you have it setup for a Go project.

```text
go get github.com/joho/godotenv
```

# DISCLAIMER
The binaries may need to be rebuild. As they are build on Ubuntu and it may also depends on how you installed Golang.
